# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Kishore G <kishore96@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-04 01:38+0000\n"
"PO-Revision-Date: 2021-11-28 20:00+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.11.90\n"

#: converterrunner.cpp:29
#, kde-format
msgid "Copy unit and number"
msgstr "எண்ணையும் அலகையும் நகலெடு"

#: converterrunner.cpp:32
#, kde-format
msgid ""
"Converts the value of :q: when :q: is made up of value unit [>, to, as, in] "
"unit.You can use the Unit converter applet to find all available units."
msgstr ""

#: converterrunner.cpp:39
#, kde-format
msgctxt "list of words that can used as amount of 'unit1' [in|to|as] 'unit2'"
msgid "in;to;as"
msgstr ""
