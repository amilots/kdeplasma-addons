# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2015, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-15 01:40+0000\n"
"PO-Revision-Date: 2023-03-01 01:25+0100\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/ui/main.qml:227
#, kde-format
msgctxt ""
"Abbreviation for result (undefined) of division by zero, max. six to nine "
"characters."
msgid "undef"
msgstr "미정"

#: package/contents/ui/main.qml:332
#, kde-format
msgctxt "@label calculation result"
msgid "Result"
msgstr "결과"

#: package/contents/ui/main.qml:361
#, kde-format
msgctxt "Text of the clear button"
msgid "C"
msgstr "C"

#: package/contents/ui/main.qml:374
#, kde-format
msgctxt "Text of the division button"
msgid "÷"
msgstr "÷"

#: package/contents/ui/main.qml:387
#, kde-format
msgctxt "Text of the multiplication button"
msgid "×"
msgstr "×"

#: package/contents/ui/main.qml:399
#, kde-format
msgctxt "Text of the all clear button"
msgid "AC"
msgstr "AC"

#: package/contents/ui/main.qml:451
#, kde-format
msgctxt "Text of the minus button"
msgid "-"
msgstr "-"

#: package/contents/ui/main.qml:503
#, kde-format
msgctxt "Text of the plus button"
msgid "+"
msgstr "+"

#: package/contents/ui/main.qml:554
#, kde-format
msgctxt "Text of the equals button"
msgid "="
msgstr "="
