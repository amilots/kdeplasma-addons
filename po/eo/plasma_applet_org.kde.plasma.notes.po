# Translation of plasma_applet_org.kde.plasma.notes.po into esperanto.
# Copyright (C) 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the kdeplasma-addons package.
# Axel Rousseau <axel@esperanto-jeunes.org>, 2009.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-26 01:34+0000\n"
"PO-Revision-Date: 2023-12-27 09:20+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: pology\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Aspekto"

#: package/contents/ui/configAppearance.qml:31
#, kde-format
msgid "%1pt"
msgstr "%1pt"

#: package/contents/ui/configAppearance.qml:37
#, kde-format
msgid "Text font size:"
msgstr "Grando de la teksta tiparo:"

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgid "A white sticky note"
msgstr "Blanka gluiĝema noto"

#: package/contents/ui/configAppearance.qml:63
#, kde-format
msgid "A black sticky note"
msgstr "Nigra gluiĝema noto"

#: package/contents/ui/configAppearance.qml:64
#, kde-format
msgid "A red sticky note"
msgstr "Ruĝa gluiĝema noto"

#: package/contents/ui/configAppearance.qml:65
#, kde-format
msgid "An orange sticky note"
msgstr "Oranĝa glueca noto"

#: package/contents/ui/configAppearance.qml:66
#, kde-format
msgid "A yellow sticky note"
msgstr "Flava gluiĝema noto"

#: package/contents/ui/configAppearance.qml:67
#, kde-format
msgid "A green sticky note"
msgstr "Verda gluiĝema noto"

#: package/contents/ui/configAppearance.qml:68
#, kde-format
msgid "A blue sticky note"
msgstr "Blua gluiĝema noto"

#: package/contents/ui/configAppearance.qml:69
#, kde-format
msgid "A pink sticky note"
msgstr "Rozkolora gluiĝema noto"

#: package/contents/ui/configAppearance.qml:70
#, kde-format
msgid "A transparent sticky note"
msgstr "Travidebla glueca noto"

#: package/contents/ui/configAppearance.qml:71
#, kde-format
msgid "A transparent sticky note with light text"
msgstr "Travidebla gluiĝema noto kun malpeza teksto"

#: package/contents/ui/main.qml:267
#, kde-format
msgid "Undo"
msgstr "Malfari"

#: package/contents/ui/main.qml:275
#, kde-format
msgid "Redo"
msgstr "Refari"

#: package/contents/ui/main.qml:285
#, kde-format
msgid "Cut"
msgstr "Tondi"

#: package/contents/ui/main.qml:293
#, kde-format
msgid "Copy"
msgstr "Kopii"

#: package/contents/ui/main.qml:301
#, kde-format
msgid "Paste"
msgstr "Alglui"

#: package/contents/ui/main.qml:307
#, kde-format
msgid "Paste with Full Formatting"
msgstr "Alglui kun Plena Formatado"

#: package/contents/ui/main.qml:314
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove Formatting"
msgstr "Forigi Formatadon"

#: package/contents/ui/main.qml:329
#, kde-format
msgid "Delete"
msgstr "Forigi"

#: package/contents/ui/main.qml:336
#, kde-format
msgid "Clear"
msgstr "Forviŝi"

#: package/contents/ui/main.qml:346
#, kde-format
msgid "Select All"
msgstr "Elekti ĉiujn"

#: package/contents/ui/main.qml:474
#, kde-format
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "Grasa"

#: package/contents/ui/main.qml:486
#, kde-format
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "Kursiva"

#: package/contents/ui/main.qml:498
#, kde-format
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "Substrekita"

#: package/contents/ui/main.qml:510
#, kde-format
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "Trastrekita"

#: package/contents/ui/main.qml:584
#, kde-format
msgid "Discard this note?"
msgstr "Ĉu forĵeti ĉi tiun noton?"

#: package/contents/ui/main.qml:585
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr "Ĉu vi certas, ke vi volas forĵeti ĉi tiun noton?"

#: package/contents/ui/main.qml:605
#, kde-format
msgctxt "@item:inmenu"
msgid "White"
msgstr "Blanka"

#: package/contents/ui/main.qml:609
#, kde-format
msgctxt "@item:inmenu"
msgid "Black"
msgstr "Nigra"

#: package/contents/ui/main.qml:613
#, kde-format
msgctxt "@item:inmenu"
msgid "Red"
msgstr "Ruĝa"

#: package/contents/ui/main.qml:617
#, kde-format
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "Oranĝkolora"

#: package/contents/ui/main.qml:621
#, kde-format
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "Flava"

#: package/contents/ui/main.qml:625
#, kde-format
msgctxt "@item:inmenu"
msgid "Green"
msgstr "Verda"

#: package/contents/ui/main.qml:629
#, kde-format
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "Blua"

#: package/contents/ui/main.qml:633
#, kde-format
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "Rozkolora"

#: package/contents/ui/main.qml:637
#, kde-format
msgctxt "@item:inmenu"
msgid "Transparent"
msgstr "Travidebla"

#: package/contents/ui/main.qml:641
#, kde-format
msgctxt "@item:inmenu"
msgid "Transparent Light"
msgstr "Travidebla Lumo"
