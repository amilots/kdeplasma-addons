# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-11 00:38+0000\n"
"PO-Revision-Date: 2023-06-17 12:05+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.03.70\n"

#: package/contents/config/config.qml:12
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "General"

#: package/contents/config/config.qml:18
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Appearance"

#: package/contents/ui/ConfigAppearance.qml:33
#, kde-format
msgctxt "@title:group"
msgid "Icon:"
msgstr "Icon:"

#: package/contents/ui/ConfigAppearance.qml:34
#, kde-format
msgctxt "@option:radio"
msgid "Website's favicon"
msgstr "Website's favicon"

#: package/contents/ui/ConfigAppearance.qml:57
#, kde-format
msgctxt "@action:button"
msgid "Change Web Browser's icon"
msgstr "Change Web Browser's icon"

#: package/contents/ui/ConfigAppearance.qml:58
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."
msgstr ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."

#: package/contents/ui/ConfigAppearance.qml:62
#, kde-format
msgctxt "@info:tooltip"
msgid "Icon name is \"%1\""
msgstr "Icon name is \"%1\""

#: package/contents/ui/ConfigAppearance.qml:97
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Choose…"

#: package/contents/ui/ConfigAppearance.qml:99
#, kde-format
msgctxt "@info:whatsthis"
msgid "Choose an icon for Web Browser"
msgstr "Choose an icon for Web Browser"

#: package/contents/ui/ConfigAppearance.qml:103
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr "Reset to default icon"

#: package/contents/ui/ConfigAppearance.qml:118
#, kde-format
msgctxt "@option:check"
msgid "Display Navigation Bar"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:21
#, kde-format
msgctxt "@option:radio"
msgid "Load last-visited page"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:25
#, kde-format
msgctxt "@title:group"
msgid "On startup:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:32
#, fuzzy, kde-format
#| msgctxt "@action:button"
#| msgid "Reload This Page"
msgctxt "@option:radio"
msgid "Always load this page:"
msgstr "Reload This Page"

#: package/contents/ui/ConfigGeneral.qml:64 package/contents/ui/main.qml:106
#, kde-format
msgctxt "@info"
msgid "Type a URL"
msgstr "Type a URL"

#: package/contents/ui/ConfigGeneral.qml:75
#, kde-format
msgctxt "@title:group"
msgid "Content scaling:"
msgstr "Content scaling:"

#: package/contents/ui/ConfigGeneral.qml:79
#, kde-format
msgctxt "@option:radio"
msgid "Fixed scale:"
msgstr "Fixed scale:"

#: package/contents/ui/ConfigGeneral.qml:113
#, kde-format
msgctxt "@option:radio"
msgid "Automatic scaling if width is below"
msgstr "Automatic scaling if width is below"

#: package/contents/ui/main.qml:67
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr "Go Back"

#: package/contents/ui/main.qml:74
#, kde-format
msgctxt "@action:button"
msgid "Go Forward"
msgstr "Go Forward"

#: package/contents/ui/main.qml:82
#, kde-format
msgctxt "@action:button"
msgid "Go Home"
msgstr ""

#: package/contents/ui/main.qml:87
#, fuzzy, kde-format
#| msgctxt "@info"
#| msgid "Type a URL"
msgid "Open Default URL"
msgstr "Type a URL"

#: package/contents/ui/main.qml:142
#, kde-format
msgctxt "@action:button"
msgid "Stop Loading This Page"
msgstr "Stop Loading This Page"

#: package/contents/ui/main.qml:142
#, kde-format
msgctxt "@action:button"
msgid "Reload This Page"
msgstr "Reload This Page"

#: package/contents/ui/main.qml:192
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Link in Browser"
msgstr "Open Link in Browser"

#: package/contents/ui/main.qml:198
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Link Address"
msgstr "Copy Link Address"

#: package/contents/ui/main.qml:260
#, kde-format
msgctxt "An unwanted popup was blocked"
msgid "Popup blocked"
msgstr "Popup blocked"

#: package/contents/ui/main.qml:261
#, kde-format
msgid ""
"Click here to open the following blocked popup:\n"
"%1"
msgstr ""
"Click here to open the following blocked popup:\n"
"%1"
