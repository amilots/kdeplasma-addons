# German translations for kdeplasma-addons package
# German translation for kdeplasma-addons.
# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-01 00:39+0000\n"
"PO-Revision-Date: 2023-10-24 12:02+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: kcm/cubeeffectkcm.cpp:35
#, fuzzy, kde-format
#| msgid "KWin"
msgid "KWin"
msgstr "KWin"

#: kcm/cubeeffectkcm.cpp:41 package/contents/ui/main.qml:42
#, kde-format
msgid "Toggle Cube"
msgstr ""

#: package/contents/ui/PlaceholderView.qml:28
#, kde-format
msgctxt "@info:placeholder"
msgid ""
"At least 3 virtual desktops are required to display the Cube, but only %1 is "
"present"
msgid_plural ""
"At least 3 virtual desktops are required to display the Cube, but only %1 "
"are present"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/PlaceholderView.qml:32
#, kde-format
msgctxt "@action:button"
msgid "Add Virtual Desktop"
msgstr ""
