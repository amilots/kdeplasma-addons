# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sairan Kikkarin <sairan@computer.org>, 2010, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2012-07-19 05:18+0600\n"
"Last-Translator: Sairan Kikkarin\n"
"Language-Team: Kazakh <kde-i18n-doc@kde.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: package/contents/config/config.qml:13
#, fuzzy, kde-format
#| msgid "Appearance"
msgctxt "@title"
msgid "Appearance"
msgstr "Көрінісі"

#: package/contents/config/config.qml:19
#, fuzzy, kde-format
#| msgid "Predefined Timers"
msgctxt "@title"
msgid "Predefined Timers"
msgstr "Мәзірдегі уақыт аралықтар"

#: package/contents/config/config.qml:24
#, kde-format
msgctxt "@title"
msgid "Advanced"
msgstr ""

#: package/contents/ui/CompactRepresentation.qml:135
#, fuzzy, kde-format
#| msgid "Timer"
msgctxt "@action:button"
msgid "Pause Timer"
msgstr "Таймер"

#: package/contents/ui/CompactRepresentation.qml:135
#, fuzzy, kde-format
#| msgid "Timer"
msgctxt "@action:button"
msgid "Start Timer"
msgstr "Таймер"

#: package/contents/ui/CompactRepresentation.qml:198
#: package/contents/ui/CompactRepresentation.qml:216
#, kde-format
msgctxt "remaining time"
msgid "%1s"
msgid_plural "%1s"
msgstr[0] ""

#: package/contents/ui/configAdvanced.qml:22
#, kde-format
msgctxt "@title:label"
msgid "After timer completes:"
msgstr ""

#: package/contents/ui/configAdvanced.qml:26
#, fuzzy, kde-format
#| msgid "Run a command:"
msgctxt "@option:check"
msgid "Execute command:"
msgstr "Команданы орындау:"

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgctxt "@title:label"
msgid "Display:"
msgstr ""

#: package/contents/ui/configAppearance.qml:35
#, fuzzy, kde-format
#| msgid "Show title:"
msgctxt "@option:check"
msgid "Show title:"
msgstr "Айдарын көрсету:"

#: package/contents/ui/configAppearance.qml:52
#, kde-format
msgctxt "@option:check"
msgid "Show remaining time"
msgstr ""

#: package/contents/ui/configAppearance.qml:58
#, fuzzy, kde-format
#| msgid "Hide seconds"
msgctxt "@option:check"
msgid "Show seconds"
msgstr "Секундтар көрсетілмесін"

#: package/contents/ui/configAppearance.qml:63
#, fuzzy, kde-format
#| msgid "Show title:"
msgctxt "@option:check"
msgid "Show timer toggle"
msgstr "Айдарын көрсету:"

#: package/contents/ui/configAppearance.qml:68
#, kde-format
msgctxt "@option:check"
msgid "Show progress bar"
msgstr ""

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgctxt "@title:label"
msgid "Notifications:"
msgstr ""

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgctxt "@option:check"
msgid "Show notification text:"
msgstr ""

#: package/contents/ui/configTimes.qml:71
#, kde-format
msgid ""
"If you add predefined timers here, they will appear in plasmoid context menu."
msgstr ""

#: package/contents/ui/configTimes.qml:78
#, kde-format
msgid "Add"
msgstr ""

#: package/contents/ui/configTimes.qml:119
#, kde-format
msgid "Scroll over digits to change time"
msgstr ""

#: package/contents/ui/configTimes.qml:126
#, kde-format
msgid "Apply"
msgstr ""

#: package/contents/ui/configTimes.qml:134
#, kde-format
msgid "Cancel"
msgstr ""

#: package/contents/ui/configTimes.qml:143
#, kde-format
msgid "Edit"
msgstr ""

#: package/contents/ui/configTimes.qml:152
#, kde-format
msgid "Delete"
msgstr ""

#: package/contents/ui/main.qml:67
#, kde-format
msgid "%1 is running"
msgstr ""

#: package/contents/ui/main.qml:69
#, kde-format
msgid "%1 not running"
msgstr ""

#: package/contents/ui/main.qml:72
#, kde-format
msgid "Remaining time left: %1 second"
msgid_plural "Remaining time left: %1 seconds"
msgstr[0] ""

#: package/contents/ui/main.qml:72
#, kde-format
msgid ""
"Use mouse wheel to change digits or choose from predefined timers in the "
"context menu"
msgstr ""

#: package/contents/ui/main.qml:89
#, kde-format
msgid "Timer"
msgstr "Таймер"

#: package/contents/ui/main.qml:90
#, kde-format
msgid "Timer finished"
msgstr ""

#: package/contents/ui/main.qml:130
#, fuzzy, kde-format
#| msgid "Start"
msgctxt "@action"
msgid "&Start"
msgstr "Старт"

#: package/contents/ui/main.qml:135
#, kde-format
msgctxt "@action"
msgid "S&top"
msgstr ""

#: package/contents/ui/main.qml:140
#, fuzzy, kde-format
#| msgid "Reset"
msgctxt "@action"
msgid "&Reset"
msgstr "Ысыру"

#, fuzzy
#~| msgid "Run a command:"
#~ msgctxt "@title:group"
#~ msgid "Run Command"
#~ msgstr "Команданы орындау:"

#, fuzzy
#~| msgid "Run a command:"
#~ msgctxt "@label:textbox"
#~ msgid "Command:"
#~ msgstr "Команданы орындау:"

#~ msgctxt "separator of hours:minutes:seconds in timer strings"
#~ msgid ":"
#~ msgstr ":"

#~ msgid "Timer Configuration"
#~ msgstr "Таймерді баптау"

#~ msgid "Stop"
#~ msgstr "Стоп"

#~ msgid "Timer Timeout"
#~ msgstr "Есеп бітті"

#~ msgid "General"
#~ msgstr "Жалпы"

#~ msgid "Plasma Timer Applet"
#~ msgstr "Plasma таймер апплеті"

#~ msgid "Actions on Timeout"
#~ msgstr "Біткендегі әрекет"

#~ msgid "Show a message:"
#~ msgstr "Жазуды көрсету:"
