# SPDX-FileCopyrightText: 2021, 2022, 2023 Xavier Besnard <xavier.besnard@kde.org>
# Simon Depiets <sdepiets@gmail.com>, 2018.
# Xavier Besnard <xavier.besnard@kde.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.keyboardindicator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-22 00:38+0000\n"
"PO-Revision-Date: 2023-08-21 14:15+0200\n"
"Last-Translator: Xavier BESNARD <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.07.90\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Touches"

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr "Afficher si activé :"

#: contents/ui/configAppearance.qml:40
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Verrouillage des majuscules"

#: contents/ui/configAppearance.qml:47
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Verr. Num."

#: contents/ui/main.qml:87
#, kde-format
msgid "Caps Lock activated"
msgstr "Verrouillage des majuscules activé"

#: contents/ui/main.qml:90
#, kde-format
msgid "Num Lock activated"
msgstr "Verrouillage du pavé numérique activé"

#: contents/ui/main.qml:98
#, kde-format
msgid "No lock keys activated"
msgstr "Aucune activation de touche de verrouillage"

#~ msgid "Num Lock"
#~ msgstr "Verr. num"

#~ msgid "%1: Locked\n"
#~ msgstr "%1 : Verrouillé\n"

#~ msgid "Unlocked"
#~ msgstr "Déverrouillé"

#~ msgid "Appearance"
#~ msgstr "Apparence"
